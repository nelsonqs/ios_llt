//
//  MappingVC.swift
//  simpleLLajtaTracking
//
//  Created by Nelson Quispe on 12/8/17.
//  Copyright © 2017 Nelson Quispe. All rights reserved.
//

import UIKit
import GoogleMaps


class MappingVC: UIViewController {

    @IBOutlet weak var itemName: UILabel!
    
    var mappingName = ""
    var lat = ""
    var long = ""
    var addressButton: UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //itemName.text = lat
        print(lat)
        loadView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func back(_ sender: Any) {
     self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func loadView() {
        let camera = GMSCameraPosition.camera(withLatitude: (self.lat as NSString).doubleValue, longitude: (self.long as NSString).doubleValue, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: (self.lat as NSString).doubleValue, longitude: (self.long as NSString).doubleValue)
        marker.title = "San francisco"
        marker.snippet = "California"
        marker.map = mapView
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Volver", style: .plain, target: self, action:"back")
        
    }

    func back() {
        
        self.dismiss(animated: true, completion: nil)
    }
}
