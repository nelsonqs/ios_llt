//
//  LoginVC.swift
//  simpleLLajtaTracking
//
//  Created by Nelson Quispe on 12/4/17.
//  Copyright © 2017 Nelson Quispe. All rights reserved.
//

import UIKit

class DataEntryTextField: UITextField, Jitterable {
    
}

class LoginButton: UIButton, Jitterable {
    
}

class Flashinglabel: UILabel, Flashable, Jitterable {
    
}

class LoginVC: UIViewController {

    @IBOutlet weak var emailField: DataEntryTextField!
    @IBOutlet weak var passwordField: DataEntryTextField!
    @IBOutlet weak var errorLabel: Flashinglabel!
    @IBOutlet weak var loginButton: LoginButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        if ((emailField.text?.isEmpty)! || (passwordField.text?.isEmpty)!) {
            emailField.jitter()
            errorLabel.flash()
            loginButton.jitter()
            errorLabel.jitter()
        } else {
            let listVC = self.storyboard?.instantiateViewController(withIdentifier: "ListVC") as! ListVC
            self.present(listVC, animated: true)
        }
    }
    
}

