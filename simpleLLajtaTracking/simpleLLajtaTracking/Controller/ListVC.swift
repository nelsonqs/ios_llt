//
//  ListVC.swift
//  simpleLLajtaTracking
//
//  Created by Nelson Quispe on 12/7/17.
//  Copyright © 2017 Nelson Quispe. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

struct Category {
    public let pid: NSInteger
    public let ln: Float
    public let rtdir: NSString
    public var subcategories = [SubCategory]()
    
    public init?(json: JSON){
        self.pid = json["pid"].intValue as NSInteger
        self.ln = json["ln"].floatValue as Float
        self.rtdir = json["rtdir"].stringValue as NSString
   
        for subcategory in json["pt"].array! {
            subcategories.append(SubCategory(json: subcategory)!)
        }
    }
}

struct SubCategory {
    public let seq: NSInteger
    public let lat: Float
    public let long: Float
    public let type: NSString
    public let stpid: NSString
    public let stpnm: NSString
    public let pdist: Float
    
    public init?(json: JSON){
       self.seq = json["seq"].intValue as NSInteger
       self.lat = json["lat"].floatValue as Float
       self.long = json["long"].floatValue as Float
       self.type = json["type"].stringValue as NSString
       self.stpid = json["stpid"].stringValue as NSString
        self.stpnm = json["stpnm"].stringValue as NSString
        self.pdist = json["pdist"].floatValue as Float
    }
}

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var ItemImage: UIImageView!
    @IBOutlet weak var ItemLabel: UILabel!
}

class ListVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var itemName = ["Pedido de gas florida","Pedido de gas america","Pedido de Gas Blanco Galindo"]
    var selectedIndex = 1
    var categories = [Category]()
    var subcategoriesLocal = [SubCategory]()
    static let bus = "bustime-response"
    static let ptr = "ptr"
    static let pt = "pt"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "CustomTableViewCell", bundle: nil), forCellReuseIdentifier: "custom")
        Alamofire.request("http://ctabustracker.com/bustime/api/v2/getpatterns?key=8U3yn68BGwKiwtNmYAPSw4qDA&pid=957&top=1&format=json", method: .get).responseJSON {
            response in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                for categoryArray in json[ListVC.bus][ListVC.ptr].array! {
                    guard let category = Category(json: categoryArray) else { continue }
                    self.categories.append(category)
                }
                self.subcategoriesLocal = self.categories[0].subcategories
                print(self.subcategoriesLocal)
                self.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
            
        }

              // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.subcategoriesLocal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CustomTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "custom") as! CustomTableViewCell
        cell.ItemLabel.text = self.subcategoriesLocal[indexPath.row].stpnm as String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("entra qui o nada")
        self.selectedIndex = indexPath.row
        performSegue(withIdentifier: "item", sender: self)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(segue.identifier!)
        if segue.identifier == "item" {
            let vc : MappingVC = segue.destination as! MappingVC
            vc.mappingName = self.subcategoriesLocal[selectedIndex].stpnm as String
            vc.lat = self.subcategoriesLocal[selectedIndex].lat.description as String
            vc.long = self.subcategoriesLocal[selectedIndex].long.description as String
         }
     }
    

}
