//
//  dataObject.swift
//  simpleLLajtaTracking
//
//  Created by Nelson Quispe on 12/9/17.
//  Copyright © 2017 Nelson Quispe. All rights reserved.
//

final class dataObject: ResponseObjectSerializable {
    let seq: Int
    let lat: String
    let lon: Float
    let typ: String
    let stpid: String
    let stpnm: String
    let pdist: Float
    
    required init?(response: HTTPURLResponse, representation: AnyObject){
        self.seq = representation.value(forKeyPath: "seq") as! Int
        
    }
    
    
}
