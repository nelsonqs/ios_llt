//
//  UIViewExtension.swift
//  simpleLLajtaTracking
//
//  Created by Nelson Quispe on 12/6/17.
//  Copyright © 2017 Nelson Quispe. All rights reserved.
//

import UIKit

extension UIView {
    func jitter(){
        let animation = CABasicAnimation(keyPath:"position")
        animation.duration = 0.05
        animation.repeatCount = 5
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint:CGPoint.init(x: self.center.x - 5.0, y: self.center.y))
        animation.toValue = NSValue(cgPoint:CGPoint.init(x: self.center.x + 5.0, y: self.center.y))
        layer.add(animation, forKey: "position")
    }
    }
