//
//  Flashable.swift
//  simpleLLajtaTracking
//
//  Created by Nelson Quispe on 12/6/17.
//  Copyright © 2017 Nelson Quispe. All rights reserved.
//

import UIKit

protocol Flashable {}
    extension Flashable where Self: UIView {
        func flash() {
            UIView.animate(withDuration: 0.3,delay:0,options: .curveEaseIn, animations: {
                self.alpha = 1.0
            }) { (animationComplete) in
                if animationComplete == true {
                    UIView.animate(withDuration: 0.3,delay:2.0, options: .curveEaseOut, animations: {
                        self.alpha = 0.0
                    }, completion: nil)
                }
                
            }
        }

    }

